from subprocess import check_output
from datetime import datetime

status = check_output('acpi')

if not status:
    print("<span color='red'><span font='FontAwesome'>\uf00d \uf240</span></span>")
    exit()

status = str(status)
battery = status[status.find(':')+2:status.find('%')+1]

now = datetime.now()

print(battery + '\t' + now.strftime('%Y-%m-%d %H:%M'))
