# stuff
alias c='cd ..'
alias cc='cd ../..'
alias ccc='cd ../../..'
alias col='column -s, -t'
alias mr='make run'
alias gs='git status'
alias gc='git commit -m'
alias gca='git commit -am'
alias ga='git add'
alias gd='git diff'
alias gb='git branch'
alias gch='git checkout'
alias gl='git log --name-status'
alias ll='ls -l -G'
# todo util
alias td='~/pierdoły/todo/todo.sh'
alias tda='td add'
alias tdd='td done'
alias tdl='td lsa'
alias todo.sh='td'

# how to use it
# include .bashrc if it exists
#if [ -f $HOME/.dotfiles/bashrc_aliases ]; then
#	. $HOME/.dotfiles/bashrc_aliases
#fi
