source ~/dotfiles/initVundle.vim
set nocompatible
set number
imap <F2> <esc> :wa<CR>
map <F2> :wa<CR>
map <F3> i<CR><esc>
map <F4> :set list!
"line >= 80

"space as leader
let mapleader="\<Space>"
nmap <leader>w :wa<cr>
nmap <leader>q :q<cr>
nmap <leader>v :vsplit<cr>
nnoremap <leader>j <C-W><C-J>
nnoremap <leader>k <C-W><C-K>
nnoremap <leader>l <C-W><C-L>
nnoremap <leader>h <C-W><C-H>

syntax on
filetype plugin indent on

set listchars=tab:→\ ,trail:·,nbsp:·"show whitespaces"
se list

imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

imap <C-s> <esc> :w<cr>
map <C-s> :w<cr>

set tabstop=2
set shiftwidth=2
set expandtab
set softtabstop=2

"this is perfect!
set scrolloff=10

function! NumberToggle()
  if(&relativenumber == 1)
    set norelativenumber
  else
    set relativenumber
  endif
endfunc
nnoremap <leader>n :call NumberToggle()<cr>
set relativenumber

colorscheme ron
match ErrorMsg '\%>80v.\+'

"NerdTree
nmap <leader>o :NERDTreeToggle<CR>

set backupdir=/tmp/vim
set directory=/tmp/vim

noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 0, 2)<CR>
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 0, 2)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 0, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 0, 4)<CR>

"Clang format
map <C-K> :py3f /usr/share/vim/addons/syntax/clang-format-3.8.py<cr>
imap <C-K> <c-o>:py3f /usr/share/vim/addons/syntax/clang-format-3.8.py<cr>
